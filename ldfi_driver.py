from ldfi_py.pbool import *
import ldfi_py.pilp
import ldfi_py.psat
import fileinput, json, sys, time

RUN_DIR='new_runs'

sys.setrecursionlimit(1000000)


traces = {}
weights = {}

tm = time.process_time()

jfd = open("mapping.json", "r")
mapping = json.loads(jfd.read())
jfd.close()

dfd = open("data.txt", "r")

#for line in fileinput.input():
for line in dfd:
    pieces = line.rstrip().split('|')
    #bin = pieces[0]
    #idf = pieces.pop(1)
    bin = pieces.pop(0)
    line = '|'.join(pieces)
    # causality heuristic goes here
    #weights[line] =  1.0 / float(int(idf) + 2) 
    if bin in traces:
        traces[bin].append(line)
    else:
        traces[bin] = [ line ]

t2 = time.process_time()
print("parsing done in %d secs"  % (t2 - tm))



conjuncts = None
for bin in traces:
    disjunction = None
    for item in traces[bin]:
        if disjunction is None:
            disjunction = Literal(item)
        else:
            disjunction = OrFormula(Literal(item), disjunction)
    if conjuncts is None:
        conjuncts = disjunction
    else:
        conjuncts = AndFormula(disjunction, conjuncts)


t2 = time.process_time()
print("formula done in %d secs"  % (t2 - tm))


#print("FORMULA is %s" % conjuncts)

if conjuncts.isCNF():

    t2 = time.process_time()
    print("check formula done in %d secs"  % (t2 - tm))
    cnf = conjuncts
else:
    print("Oh dear, not CNF")
    cnf = CNFFormula(conjuncts)


t2 = time.process_time()
print("CNF done in %d secs"  % (t2 - tm))

#s = ldfi_py.pilp.Solver(cnf)

#for w in weights:
#    print "key %s weight %f" % (w, weights[w])

#print("CNF is %s" % cnf)

s = ldfi_py.pilp.ProbSolver(cnf, weights)

#for s in s.solutions():
#   print s         

fst = next(s.solutions())


t2 = time.process_time()
print("solving done in %d secs"  % (t2 - tm))

for fault in fst:
    #cut = str(fault).split("|")
    faultstr= str(fault)
    print("Chosen fault: " + faultstr)
    for k in mapping[faultstr]:
        # convert to ascii, oy
        #k = k.encode('ascii', 'ignore')
        fault = "FAULT %s %s" % (mapping[faultstr][k]['tid'], mapping[faultstr][k]['eid'])
        #print fault
        fd = open(RUN_DIR + "/" + k, "r")
        # rethink this.
        content = fd.read()
        fd.close()
        wfd = open("faults/" + k, "w")
        wfd.write(content)
        wfd.write(fault)
        wfd.close()
