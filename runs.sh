#!/bin/bash

RUNS=new_runs

for i in `ls $RUNS`; do
    rm example.db
    python parser.py $RUNS/$i
done
