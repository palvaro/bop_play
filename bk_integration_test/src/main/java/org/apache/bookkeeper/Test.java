package org.apache.bookkeeper;


import static org.junit.Assert.*;                
  
import com.google.common.primitives.Ints;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import org.apache.bookkeeper.conf.ClientConfiguration;
import org.apache.bookkeeper.client.BookKeeper;
import org.apache.bookkeeper.client.LedgerEntry;
import org.apache.bookkeeper.client.LedgerHandle;
import org.apache.bookkeeper.client.BKException;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;

public class Test extends LeaderSelectorListenerAdapter implements Closeable {

    final static String ZOOKEEPER_SERVER = "127.0.0.1:2181";
    final static String ELECTION_PATH = "/leader-election";
    final static byte[] PASSWD = "password".getBytes();
    final static String LOG = "/log";
    final static int numEntries = 1000;

    CuratorFramework curator;
    LeaderSelector leaderSelector;
    BookKeeper bookkeeper;

    volatile boolean leader = false;


    Test() throws Exception {
        curator = CuratorFrameworkFactory.newClient(ZOOKEEPER_SERVER,
                2000, 10000, new ExponentialBackoffRetry(1000, 3));
        curator.start();
        curator.blockUntilConnected();

        leaderSelector = new LeaderSelector(curator, ELECTION_PATH, this);
        leaderSelector.autoRequeue();
        leaderSelector.start();

        ClientConfiguration conf = new ClientConfiguration()
            .setZkServers(ZOOKEEPER_SERVER).setZkTimeout(30000);
        bookkeeper = new BookKeeper(conf);
    }

    @Override
    public void takeLeadership(CuratorFramework client)
            throws Exception {
        synchronized (this) {
            leader = true;
            try {
                while (true) {
                    this.wait();
                }
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                leader = false;
            }
        }
    }

    @Override
    public void close() {
        leaderSelector.close();
        curator.close();
    }

    void writeEntries() throws Exception {
        List<Long> ledgers = new ArrayList<Long>();
        
        // Create a new ledger, the numbers represent ensemble, the write quorum and the ack quorum. 
        LedgerHandle writelh = bookkeeper.createLedger(3, 3, 2,
            BookKeeper.DigestType.MAC, PASSWD);
         long ledgerId;

         // Stores the id of the current ledger
         ledgerId = writelh.getId();
         ledgers.add(ledgerId);
         byte[] ledgerListBytes = listToBytes(ledgers);
         try {
             curator.create().forPath(LOG, ledgerListBytes);
         } catch (KeeperException.NodeExistsException nee) {
                System.out.println(nee.toString());
                System.out.println("Please restart the bookies, exiting...");
                System.exit(1);
         }

         if (leader) {
             // Write 1000 entries to the current ledger
             for (int i = 0; i < numEntries; i++) {
                 writelh.addEntry(("entry-" + i).getBytes());
                 //System.out.println("Value = " + i 
                 //                  + ", isLeader = " + leader);
             }
         }
         writelh.close(); 
    }

    void readEntries() throws Exception {
        // First, get the ledger id 
        Stat stat = new Stat();
        byte[] ledgerListBytes = curator.getData().storingStatIn(stat).forPath(LOG);
        List<Long> ledgers = listFromBytes(ledgerListBytes);

        // Open and read from the ledger
        LedgerHandle readlh =  bookkeeper.openLedger(ledgers.get(0),
                        BookKeeper.DigestType.MAC, PASSWD);
        long lac = readlh.getLastAddConfirmed();
        int i = 0;
        Enumeration<LedgerEntry> entries = readlh.readEntries(0, lac);
        while (entries.hasMoreElements()) {
            LedgerEntry e = entries.nextElement();
            String readBack = new String(e.getEntry());
            assertEquals(readBack, "entry-" + i++);
        }
        assertEquals(i, numEntries);
    }

    /*
        Integration test for bookkeeper's durability. 
        We write 1000 entries to a ledger and close it. Then, we sleep for 10s and check
        the entries are still there.
    */
    public static void main(String[] args) throws Exception {
	
        
        Test client1 = new Test();
        System.out.println("Creating a new ledger and writing 1000 entries");
        client1.writeEntries();
        client1.close();
        System.out.println("Finished writing entries, ending current session");
        System.out.println("Sleeping for 10s");
        Thread.sleep(10000);

        System.out.println("Opening a new session");
        Test client2 = new Test();
        System.out.println("Checking durability of entries");
        client2.readEntries();
        System.out.println("TEST PASSED");
        System.exit(0);
    }

    static byte[] listToBytes(List<Long> ledgerIds) {
        ByteBuffer bb = ByteBuffer.allocate((Long.SIZE*ledgerIds.size())/8);
        for (Long l : ledgerIds) {
            bb.putLong(l);
        }
        return bb.array();
    }

    static List<Long> listFromBytes(byte[] bytes) {
        List<Long> ledgerIds = new ArrayList<Long>();
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        while (bb.remaining() > 0) {
            ledgerIds.add(bb.getLong());
        }
        return ledgerIds;
    }
}
