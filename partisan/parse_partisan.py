import sys,re
import json
import sqlite3

def quoted(st):
    #if is_int.match(st):
    if str(st).isdigit():
        return st
    else:
        return '"' + str(st) + '"'

class SQLIt():
    def __init__(self, messages, enters, exists):
        self.messages = messages
        self.enters = enters
        self.exits = exits

        
    def tbl_ddl(self, tbl, row):
        ret = "CREATE TABLE IF NOT EXISTS " + tbl + " ( \n"
        for k in row.keys():
            ret += "\t" + k
            val = str(row[k])
            #if is_int.match(val):
            if val.isdigit():
                ret += " integer,"
            else:
                ret += " varchar(255),"

        ret = ret[:-1]
        ret +=  "); "
        ret += "DELETE FROM " + tbl + ";"
        return ret

    def ddl(self):
        ret = ''
        ret +=  self.tbl_ddl("messages", self.messages[0]) 
        ret +=  self.tbl_ddl("enters", self.enters[0]) 
        ret +=  self.tbl_ddl("exits", self.exits[0]) 
       
        return ret
    
        

    def data_items(self, tblname, collection):
        ret = ''
        for item in collection:
            ret += "INSERT INTO " + tblname + " ( " + ','.join(item.keys()) + " ) VALUES ( " + ','.join(map(lambda x: quoted(x), item.values())) + ");"
        return ret
            


    def data(self):
        ret = ''
        ret += self.data_items("messages", self.messages)
        ret += self.data_items("enters", self.enters)
        ret += self.data_items("exits", self.exits)
        return ret


fd = open(sys.argv[1], "r")
trace = json.loads(fd.read())
fd.close()

#print "trace has %d elements" % len(trace)
messages = []
enters = []
exits = []

rcv_re = re.compile('\{forward_message,\s*([a-zA-Z0-9\-]+),\s*(.+?)\}')

for idx, elem in enumerate(trace):
    type = elem['type']
    elem["id"] = str(idx)
    if type == 'pre_interposition_fun':
        itype = elem['interposition_type']
        #if itype == 'receive_message':
            #print "RECEIVE %s" % elem['message_payload']
            #match = rcv_re.match(elem['message_payload'])
            #print "MATCH IS %s" % match
        messages.append(elem)
    elif type == "enter_command":
        enters.append(elem)
    elif type == "exit_command":
        exits.append(elem)
    else:
        #print "WHAA? %s" % type
        #skip
        pass

sql = SQLIt(messages, enters, exits)

#print sql.ddl()
#print sql.data()

conn = sqlite3.connect('example.db')
c = conn.cursor()
c.executescript(sql.ddl())
c.executescript(sql.data())

c.executescript("""
create view if not exists messages_clean as select *, substr(message_payload, instr(substr(message_payload,2),'{') + 1, length(substr(message_payload, instr(substr(message_payload,2),'{') + 1)) - 1) as clean_payload from messages;
""")

c.executescript("""
create view rendezvous as 
select m2.origin_node, m2.tracing_node, m1.id id1, m2.id id2 from messages_clean m1, messages_clean m2 where m1.interposition_type ='forward_message' and m2.interposition_type='receive_message' and m1.message_payload = m2.clean_payload and m2.origin_node = m1.tracing_node and m1.origin_node=m2.tracing_node;
""")

c.executescript("""
Create view hb as
With recursive happens_before(node, id) as (
    /* eg, find the causal history of event 50 in thread 3 */
    /* first, it involves itself. */
    VALUES('node_3@parrhesia', 991)
    UNION
    /* at any step, it certainly also involves prior events in the same thread for anything
       That is already in the causal history */
Select  x.n1 node, x.id1 id from (
        select tracing_node n1, id id1, '' n2, -1 id2, 'l' label from messages_clean 
UNION 
/* as well as, for any message receive event in the current history, its corresponding
        Send event */
Select tracing_node n1, id1, origin_node n2, id2, 'r' label
    from rendezvous 
) x, happens_before h
        /* SQL made me mux them, because it's a fuck, so demux them. */
    where (x.label = 'l' and x.n1 = h.node and x.id1 < h.id) 
OR
        (x.label = 'r' and x.n2 = h.node and x.id2 = h.id)
)
Select * from happens_before;
""")


# select count(*) from (select m2.origin_node, m2.tracing_node, m1.id, m2.id from messages_clean m1, messages_clean m2 where m1.interposition_type ='forward_message' and m2.interposition_type='receive_message' and m1.message_payload = m2.clean_payload and m2.origin_node = m1.tracing_node);
