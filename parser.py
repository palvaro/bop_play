import re, sys, os
import sqlite3
import struct, socket, json


syscall_re = re.compile("SYSCALL (?P<internal_uuid>[0-9-]+) (?P<issuing_thread_tid>[0-9-]+) (?P<issuing_process_pid>[0-9-]+) (?P<local_id>[0-9-]+) (?P<syscall_number>[0-9-]+) (?P<arg0>[0-9-]+) (?P<arg1>[0-9-]+) (?P<arg2>[0-9-]+) (?P<arg3>[0-9-]+) (?P<arg4>[0-9-]+) (?P<arg5>[0-9-]+) (?P<retval>[0-9-]+) (?P<was_successful>[0-9-]+)")

sockop_re = re.compile("\s+sockop (?P<socket_internal_id>\d+)")
serversock_re = re.compile("\s+serversock (?P<socket_internal_id>\d+)")

sock_re = re.compile("SOCK (?P<internal_id>[0-9-]+) (?P<flags>[0-9-]+) (?P<file_descriptor>[0-9-]+) (?P<owner>[0-9-]+) (?P<creator>[0-9-]+)")

#process_re = re.compile("PROCESS (?P<internal_id>[0-9-]+) (?P<process_id>[0-9-]+) (?P<exit_code>[0-9-]+) (?P<did_exit>[0-9-]+) (?P<thread_cnt>[0-9-]+>) (?P<command_line>[a-zA-Z0-9._\-/]+)") 
process_re = re.compile("PROCESS (?P<internal_id>[0-9-]+) (?P<process_id>[0-9-]+) (?P<exit_code>[0-9-]+) (?P<did_exit>[0-9-]+) (?P<thread_cnt>\d+) (?P<command_line>\S+)") 

thread_re = re.compile("THREAD (?P<internal_id>[0-9-]+) (?P<thread_id>[0-9-]+) (?P<process_internal_id>[0-9-]+) (?P<event_cnt>[0-9-]+)")

event_re = re.compile("\s*EVENT (?P<internal_perthread_id>[0-9-]+) (?P<thread_internal_id>[0-9-]+) (?P<syscall_internal_id>[0-9-]+) (?P<was_syscall_entry>[0-9-]+) (?P<additional_parent_cnt>[0-9-]+)")

subevent_re = re.compile("\s+event (?P<parent_thread_event_id>[0-9-]+) (?P<parent_thread_id>[0-9-]+)")

# addr sockaddr_in (16)53940:127.0.0.1
peer_re = re.compile("\s+(?P<type>peer|addr)\s+sockaddr_in\s+\((?P<descriptor>\d+)\)(?P<port>\d+):(?P<ip>[0-9\.]+)")



conn_re = re.compile("CONN (?P<internal_id>[0-9-]+) (?P<connector_socket_id>[0-9-]+) (?P<acceptor_socket_id>[0-9-]+) (?P<connect_syscall_id>[0-9-]+) (?P<accept_syscall_id>[0-9-]+)")

is_int = re.compile('[0-9\-]+')


def quoted(st):
    #if is_int.match(st):
    if st.isdigit():
        return st
    else:
        return '"' + st + '"'

class SQLIt():
    def __init__(self, syscalls, processes, threads, events, sockets, connections, rendezvous, calltable, peerings, sockops):
        self.syscalls = syscalls
        self.processes = processes
        self.threads = threads
        self.events = events
        self.sockets = sockets
        self.connections = connections
        self.peerings = peerings
        self.sockops = sockops

        self.rendezvous = []
        self.calltable = []

        for s, event, thread in rendezvous:
            s["child_event"] = str(event)
            s["child_thread"] = str(thread)
            self.rendezvous.append(s)

        for k in calltable.syscalls:
            self.calltable.append({'syscall_number': str(k), 'syscall_name': calltable.syscalls[k] })

    def tbl_ddl(self, tbl, row):
        ret = "CREATE TABLE IF NOT EXISTS " + tbl + " ( \n"
        for k in row.keys():
            ret += "\t" + k
            val = row[k]
            #if is_int.match(val):
            if val.isdigit():
                ret += " integer,"
            else:
                ret += " varchar(255),"

        ret = ret[:-1]
        ret +=  "); "
        ret += "DELETE FROM " + tbl + ";"
        return ret

    def ddl(self):
        ret = ''
        ret +=  self.tbl_ddl("syscalls", self.syscalls[0]) 
        ret +=  self.tbl_ddl("processes", self.processes[0])
        ret +=  self.tbl_ddl("threads", self.threads[0])
        ret +=  self.tbl_ddl("events", self.events[0][0])
        ret +=  self.tbl_ddl("rendezvous", self.rendezvous[0])
        ret +=  self.tbl_ddl("calltable", self.calltable[0])
        #print "peerings dll is %s" % self.tbl_ddl("peerings", self.peerings[0])
        ret += self.tbl_ddl("peerings", self.peerings[0])
        ret += self.tbl_ddl("sockops", self.sockops[0])

        #print self.tbl_ddl("syscalls", self.syscalls[0])

       
        return ret
    
        

    def data_items(self, tblname, collection):
        ret = ''
        for item in collection:
            ret += "INSERT INTO " + tblname + " ( " + ','.join(item.keys()) + " ) VALUES ( " + ','.join(map(lambda x: quoted(x), item.values())) + ");"
        return ret
            


    def data(self):
        ret = 'begin transaction;\n'
        ret += self.data_items("syscalls", self.syscalls)
        ret += self.data_items("processes", self.processes)
        ret += self.data_items("threads", self.threads)
        ret += self.data_items("rendezvous", self.rendezvous)
        ret += self.data_items("sockops", self.sockops)
        ret += self.data_items("calltable", self.calltable)
        for e in self.events:
            ret += self.data_items("events", self.events[e])
        #print "OK %s" % self.data_items("peerings", self.peerings)
        ret += self.data_items("peerings", self.peerings)
	ret += "\nCOMMIT;"
        return ret


class Syscalls():
    def __init__(self):
        self.syscalls = {}
        kvre = re.compile('\[([0-9-]+)\] = "([^"]+)"')
        with open("scnames.h") as file:
            for line in file:
                match = kvre.match(line)
                if match:
                    #print "assign to " + match.group(1) + " :: " + match.group(2)
                    self.syscalls[int(match.group(1))] = match.group(2)

    def resolve(self, id):
        return self.syscalls[id]




def event_to_syscall(calltable, syscalls, events, thread, id):
   l = events[thread][id]
   sys = syscalls[int(l["syscall_internal_id"])]
   return sys

def thread_to_commandline(threads, processes, thread):
    t = threads[thread]
    p = processes[int(t["process_internal_id"])]
    return p["command_line"] + "(" + t["process_internal_id"] + ")"




syscalls = []
socks = []
processes = []
threads = []
events = {}
conns = []
rendezvous = []
sockops = []
peerings = []


current_thread_id = -1
current_event_id = -1
context = [-1, -1]
sock_cnt = -1 

calltable = Syscalls()

with open(sys.argv[1], "r") as file:
    for line in file:
        syscall = syscall_re.match(line)
        sock = sock_re.match(line)
        process = process_re.match(line)
        thread = thread_re.match(line)
        event = event_re.match(line)
        subevent = subevent_re.match(line)
        conn = conn_re.match(line)
        sockop = sockop_re.match(line)
        peer = peer_re.match(line)

        #print "LINE " + line

        if syscall:
            s  = syscall.groupdict()
            syscalls.append(s)
            current_syscall = s["internal_uuid"]
        elif sock:
            # obtain some context about the socket here....
            s = sock.groupdict()
            sock_cxt = int(s["internal_id"])
            socks.append(sock.groupdict())
            current_sock_info = sock.groupdict()
        elif sockop:
            s = sockop.groupdict()
            s['syscall_uuid'] = current_syscall
            #print "SOCKOP! "  + str(s) + " at "  + str(current_syscall)
            #sockops[current_syscall] = int(s["socket_internal_id"])
            sockops.append(s)
        elif process:
            processes.append(process.groupdict())
        elif thread:
            t = thread.groupdict()
            threads.append(t)
            current_thread_id = int(t["internal_id"])
            if t["event_cnt"] > 0:
                #print "ADDIT"
                events[current_thread_id] = []
        elif event:
            e = event.groupdict()
            events[int(e['thread_internal_id'])].append(e)
            context = [int(e["internal_perthread_id"]), int(e["thread_internal_id"])]
        elif subevent:
            # where the action happens.  just print for now
            #print "SUBEVENT!  there are " + str(len(syscalls)) + " syscalls"
            #print "CXT %s" % context
            s = subevent.groupdict()
            rendezvous.append([s, context[0], context[1]])
        elif peer:
            #print "PEER!"
            #peer_info = peer.groupdict().update(current_sock_info)
            #peer_info = {**pi, **current_sock_info}
            peer_info = peer.groupdict()
            peer_info['owner'] = current_sock_info['owner']
            peer_info['creator'] = current_sock_info['creator']
            peer_info['sock_iid'] = current_sock_info['internal_id']
            port = int(peer_info['port'])
            #print "port is %d" % port
            #packed = struct.pack('>I', port)
            #unpacked = struct.unpack('<I', packed)
            #print "unpacked port is %d" % unpacked
            unpacked = socket.ntohs(port)
            #print "unpacked port is %d" % unpacked
            peer_info['port'] = str(unpacked)

            #print "info is %s" % peer_info
            peerings.append(peer_info)
        elif conn:
            conns.append(conn.groupdict())

#print "OK peerings is %s" % peerings

sql = SQLIt(syscalls, processes, threads, events, socks, conns, rendezvous, calltable, peerings, sockops)

if len(sys.argv) == 3:
    db = sys.argv[2]
else:
    db = 'example.db'

conn = sqlite3.connect(db, isolation_level=None)



c = conn.cursor()
c.executescript(sql.ddl())


print "OK-1"
#c.execute("begin transaction")
c.executescript(sql.data())
#c.execute("commit")

print "OK0"


#print sql.ddl()
#print sql.data()

#print "/*"



#for s, c0, c1 in rendezvous:
    # the target event
    #tgt_call = event_to_syscall(calltable, syscalls, events, c1, c0)
    #callname = calltable.resolve(int(tgt_call["syscall_number"]))  
    
    #eref = events[c1][c0] 
    #print "EREF " + eref["was_syscall_entry"]
    
    #eref2 = events[int(s['parent_thread_id'])][int(s['parent_thread_event_id'])]
    #print "EREF2 " + eref2["was_syscall_entry"]
 
    #print "t is " + callname + " :: " + str(tgt_call) + " socket " + str(sockops.get(int(tgt_call["internal_uuid"]), "NONE"))
    #src_call = event_to_syscall(calltable, syscalls, events, int(s['parent_thread_id']), int(s['parent_thread_event_id']))

    #callname2 = calltable.resolve(int(src_call["syscall_number"]))  
    #print "s is " + callname + " :: " + str(src_call) + " socket " + str(sockops.get(int(src_call["internal_uuid"]), "NONE"))

    
    #print thread_to_commandline(threads, processes, int(s['parent_thread_id'])) + " : " + callname2 + " ---> " + thread_to_commandline(threads, processes, c1)  + " : " + callname

    
    #t = threads[int(s['parent_thread_id'])]
    #print "lookup " + t["process_internal_id"]
    #print "OROC " + str(processes)
    #p = processes[int(t["process_internal_id"])]
    
    #src_cmd = p["command_line"]
    #print "SRC " + src_cmd

    #print src_call["issuing_process_pid"] + " : " + callname2 + " ---> " + tgt_call["issuing_process_pid"] + " : " + callname

print "OK1"



c.executescript("""
create view if not exists syscall_n as select s.*, c.syscall_name from syscalls s, calltable c where s.syscall_number = c.syscall_number; 

create view if not exists event_p as select e.*, p.command_line from events e, threads t, processes p
    where e.thread_internal_id = t.internal_id
    and t.process_internal_id = p.internal_id;
""")

c.executescript("""
create table mapping(ip varchar(50), port int, rid int);
create table ip_ports as select distinct ip, port from peerings;
""")

c.execute("select distinct ip from ip_ports");
for tup in c.fetchall():
    indx = 0
    c3 = conn.cursor()
    c3.execute("select port from ip_ports where ip = '" + tup[0] + "'")
    for t2 in c3.fetchall():
        c4 = conn.cursor()
	stri = "insert into mapping values (" + ",".join(["'" + tup[0] + "'", str(t2[0]), str(indx)]) + ")"
	print "str is " + stri
	c4.execute(stri)
	indx += 1



# TODO: fix port_alt logic to index rather than do arithmetic
#c.executescript("""
#create view if not exists context as
#select p.ip, p.port, e.thread_internal_id tid,  e.internal_perthread_id eid, c.syscall_name, e.command_line, CASE WHEN port > 10000 then (m.mp - port) ELSE port END port_alt
#    from peerings p, sockops s, syscall_n c,event_p e, (select ip, max(port) mp from peerings group by ip) m 
#        where p.sock_iid = s.socket_internal_id 
#        and s.syscall_uuid = c.internal_uuid
#        and e.syscall_internal_id = c.internal_uuid
#        and m.ip = p.ip
#        and p.type = 'addr';
#""")

c.executescript("""
create view if not exists context as
select p.ip, p.port, e.thread_internal_id tid,  e.internal_perthread_id eid, c.syscall_name, e.command_line, CASE WHEN p.port > 10000 then m.rid ELSE p.port END port_alt
    from peerings p, sockops s, syscall_n c,event_p e, mapping m 
        where p.sock_iid = s.socket_internal_id 
        and s.syscall_uuid = c.internal_uuid
        and e.syscall_internal_id = c.internal_uuid
        and m.ip = p.ip
	and m.port = p.port
        and p.type = 'addr';
""")



c.executescript("""
create view if not exists rendz as
        select r.parent_thread_id parent_tid, r.parent_thread_event_id parent_eid, r.child_thread child_tid, r.child_event child_eid, c1.ip child_ip, c1.port child_port, c1.syscall_name child_syscall, c1.command_line child_command, c2.ip parent_ip, c2.port parent_port, c2.syscall_name parent_syscall, c2.command_line parent_command, c1.port_alt child_port_alt, c2.port_alt parent_port_alt
    from rendezvous r, context c1, context c2 
        where r.child_thread = c1.tid and r.child_event = c1.eid and r.parent_thread_id = c2.tid and r.parent_thread_event_id = c2.eid order by r.parent_thread_id, r.parent_thread_event_id, r.child_thread, r.child_event;
""")


c.execute("""
    select distinct parent_ip, parent_port_alt, child_ip, child_port_alt from rendz;
""")


disj = []
# TODO: grab one if it already exists
#map = {}
if os.path.isfile("mapping.json"):
    fd = open("mapping.json", "r")
    map = json.loads(fd.read())
    fd.close()
else:
    map = {}

print "OK2"
for tup in c.fetchall():
    (pip, pp, cip, cp) = tup
    sql = " select parent_tid, parent_eid, parent_ip, parent_port_alt, child_ip, child_port_alt from rendz where parent_ip = '%s' and parent_port_alt = %s and child_ip = '%s' and child_port_alt = %s;" % (pip, pp, cip,cp)
    #print "SQL is %s" % sql
    c2 = conn.cursor()
    c2.execute(sql)
    #map['abstract'] = { 'parent_ip': parent_ip, 'parent_port': str(parent_port), 'child_ip': child_ip, 'child_port': str(child_port), 'index': str(idx) } 
    

    for idx, tup in enumerate(c2.fetchall()):
        (tid, eid, parent_ip, parent_port, child_ip, child_port) = tup
        #map = {"ground": {'tid': tid, 'eid': eid, 'trace': sys.argv[1].split('/')[1]}, "abstract": {'parent_ip': parent_ip, 'parent_port': str(parent_port), 'child_ip': child_ip, 'child_port': str(child_port), 'index': str(idx)}}
        disj.append('|'.join([parent_ip, str(parent_port), child_ip, str(child_port), str(idx)]))
        

jsn_fd = open("mapping.json", "w")
data_fd = open("data.txt", "a+")

fn = sys.argv[1].split('/')[1]

print "OK3"

for d in disj:
    print d
    if d not in map:
        map[d] = {}
    #map[d].append({'tid': tid, 'eid': eid, 'trace': sys.argv[1].split('/')[1]})
    map[d][fn] = { 'tid': tid, 'eid': eid }
    data_fd.write(fn + '|' + d)
    data_fd.write("\n")

jsn_fd.write(json.dumps(map))
jsn_fd.close()
data_fd.close()
